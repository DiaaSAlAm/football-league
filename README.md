# Football League

Project Overview

Football league is a simple app consisting of two screens:
1- Premier League Teams:
- Should show the list of the premier league’s teams for current season. 
2- League Team’s Info:
- Should show the team Info (name, players’ names, website)

APIs:
Note: please generate an API-Key before you start. URL: https://www.football-data.org/client/register League Teams: /v2/competitions/{id}/teams
Team Info: /v2/teams/{id}

Features
* Getting list of Premier League Teams.
* Show specific team info
* Unit test

Requirements
* Xcode 12.3
* iOS 13+ 
* Swift 5+  

App Structure
* Use VIPER architecture pattern
* SOLID principles, OOP and POP applied
* Storyboards/Nibs.
* Auto Layout.
* URLSession Network part. 
* Add Configuration Environment for both Development Release and Production Release
* Using Native code for the Network layer, Toast Message, and  UIActivityIndicatorView
* Create the main branch for production, development for developers team, Create a branch for each feature and merge it with development then merge development with the main branch
