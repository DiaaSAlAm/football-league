//
//  BaseResponse.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

class BaseResponse<T: Codable>: Codable {
    var status: String?
    var data: T?
    var error: String?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case data = "data"
        case error = "error"
    }
}
