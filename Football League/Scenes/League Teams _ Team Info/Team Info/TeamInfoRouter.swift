//
//  TeamInfoRouter.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import UIKit
import SafariServices
 
final class TeamInfoRouter: TeamInfoRouterProtocol { //Router is only one take dicret instance
    

    weak var viewController: UIViewController?
    var baseRouter: BaseRouterProtocol? = BaseRouter()
    
    static func createModule(teamId: Int) -> UIViewController {
        let view = UIStoryboard(name: StoryboardNames.main, bundle: nil).instantiateViewController(withIdentifier: "\(TeamInfoVC.self)") as! TeamInfoVC
        let interactor = TeamInfoInteractor()
        let router = TeamInfoRouter()
        let presenter = TeamInfoPresenter(view: view, interactor: interactor, router: router, teamId: teamId)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }
    
    func openSafariServices(_ urlString: String){
        guard let url = URL(string: urlString) else {return}
        let svc = SFSafariViewController(url: url)
        viewController?.present(svc, animated: true, completion: nil)
    }

}
