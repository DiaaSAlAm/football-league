//
//  TeamInfoVC.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import UIKit
 
final class TeamInfoVC: BaseViewController, TeamInfoViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var teamNameLabel: UILabel!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    
    //MARK: - Properties
    var presenter: TeamInfoPresenterProtocol!
    private let cellIdentifier = CellIdentifiers.commonCell
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCollectionView()
        apiCall()
        bind()
    }
    
    //MARK: - Register  CollectionView Cell
    fileprivate func registerCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    //MARK: - API's Call
    private func apiCall() {
        presenter.getTeamInfo()
    }
    
    //MARK: - bind Data
    private func bind() {
        presenter.teamInfoModel.observe(on: self) { [weak self] (model) in
            self?.teamNameLabel.text = model?.name
            self?.collectionView.reloadData()
        }
        presenter.loadingIndicator.observe(on: self) { [weak self] (showLoading) in
            switch showLoading {
            case .showIndicator:
                self?.showLoadingIndicator()
            case .hiddenIndicator:
                self?.hideLoadingIndicator()
            }
        }
    }
    
    //MARK: - IBOutlets
    @IBAction
    func didTappedOpenWebButton(_ sender: UIButton) {
        presenter.openSafariServices()
    }
}

 

//MARK: - UICollectionView Data Source
extension TeamInfoVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.teamInfoModel.value?.squad?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,for: indexPath) as! CommonCell
        presenter.configureCell(cell: cell, indexPath: indexPath)
        return cell
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension TeamInfoVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        return CGSize(width: width / 2 , height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


 
