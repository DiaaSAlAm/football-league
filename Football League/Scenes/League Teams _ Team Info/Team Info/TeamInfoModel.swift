//
//  TeamInfoModel.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//
 
 
import Foundation

// MARK: - TeamInfoModel
struct TeamInfoModel: Codable {
    let id: Int?
    let name: String?
    let website: String?
    let squad: [Squad]?
}

// MARK: - Squad
struct Squad: Codable {
    let id: Int?
    let name: String?
}
