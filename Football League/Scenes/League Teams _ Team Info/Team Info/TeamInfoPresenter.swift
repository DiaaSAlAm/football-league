//
//  TeamInfoPresenter.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import UIKit

final class TeamInfoPresenter: TeamInfoPresenterProtocol, TeamInfoInteractorOutputProtocol {
    
    weak var view: TeamInfoViewProtocol?
    private let interactor: TeamInfoInteractorInputProtocol
    private var router: TeamInfoRouterProtocol
    private let teamId: Int
    var loadingIndicator: Observable<LoadingIndicator> = Observable(.hiddenIndicator)
    var teamInfoModel: Observable<TeamInfoModel?> = Observable(nil)
    
    
    init(view: TeamInfoViewProtocol, interactor: TeamInfoInteractorInputProtocol, router: TeamInfoRouterProtocol,teamId: Int) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.teamId = teamId
    }
    
    func getTeamInfo() {
        loadingIndicator.value = .showIndicator
        interactor.getTeamInfo(teamId: teamId)
    }
    
    func fetchedSuccessfuly(model: TeamInfoModel) {
        loadingIndicator.value = .hiddenIndicator
        teamInfoModel.value = model
    }
    
    func fetchingFailed(withError error: String) {
        loadingIndicator.value = .hiddenIndicator
        router.baseRouter?.showMessage(message: error, messageKind: .error)
    }
    
    func showMessage(message: String) {
        router.baseRouter?.showMessage(message: message, messageKind: .success)
    }
    
    func configureCell(cell: CellViewProtocol, indexPath: IndexPath) {
        guard let model = teamInfoModel.value?.squad?.getElement(at: indexPath.row) else {return}
        let viewModel = CommonViewModel(name: model.name ?? "")
        cell.configureCell(viewModel: viewModel)
    }
    
    func openSafariServices(){
        guard let urlString = teamInfoModel.value?.website else {return}
        router.openSafariServices(urlString)
    }
     
}
