//
//  TeamInfo.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import UIKit

protocol TeamInfoViewProtocol: class { //View Conteroller
    var presenter: TeamInfoPresenterProtocol! { get set }
}

protocol TeamInfoPresenterProtocol: class { // Logic
    var view: TeamInfoViewProtocol? { get set }
    func configureCell(cell: CellViewProtocol, indexPath: IndexPath)
    var loadingIndicator: Observable<LoadingIndicator> { get }
    var teamInfoModel: Observable<TeamInfoModel?> { get }
    func getTeamInfo()
    func openSafariServices()
     
}

protocol TeamInfoInteractorInputProtocol: class { // func do it from presenter
     var presenter: TeamInfoInteractorOutputProtocol? { get set }
    func getTeamInfo(teamId: Int)
    
}

protocol TeamInfoInteractorOutputProtocol: class { // it's will call when interactor finished
    func fetchedSuccessfuly(model: TeamInfoModel)
    func fetchingFailed(withError error: String)
    func showMessage(message: String)
}

protocol TeamInfoRouterProtocol { // For Segue
    var baseRouter: BaseRouterProtocol? { get set }
    func openSafariServices(_ urlString: String)
}
