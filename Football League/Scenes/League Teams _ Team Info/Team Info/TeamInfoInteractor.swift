//
//  TeamInfoInteractor.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import Foundation

final class TeamInfoInteractor: TeamInfoInteractorInputProtocol {

    weak var presenter: TeamInfoInteractorOutputProtocol?
    private let leagueTeamNetwork: LeagueTeamAPIProtocol = LeagueTeamAPI()
    
    func getTeamInfo(teamId: Int){
        leagueTeamNetwork.getTeamInfo(teamId: teamId) { [weak self] (result) in
          guard let self = self else {return}
          switch result {
          case .success(let model):
            guard model != nil else {
                self.presenter?.fetchingFailed(withError: StaticMessages.genericErrorMessage)
                return}
            self.presenter?.fetchedSuccessfuly(model: model!)
          case .failure(let error):
              let err = error.localizedDescription
              print(err)
            self.presenter?.fetchingFailed(withError: err)
          }
        }
    }
     
}
