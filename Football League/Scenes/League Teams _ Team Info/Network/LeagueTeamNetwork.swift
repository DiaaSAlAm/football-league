//
//  LeagueTeamNetwork.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import Foundation

enum LeagueTeamNetwork {
    case getLeagueTeams
    case getTeamInfo(teamId: Int)
}

extension LeagueTeamNetwork: TargetType {
    
    var baseURL: String {
        return Environment.rootURL.absoluteString
    }
    
    var apiKey: String {
        return Environment.apiKey
    }
    
    var path: String {
        switch self {
        case .getLeagueTeams:
            return "competitions/2021/teams"
        case .getTeamInfo(let teamId):
            return "teams/\(teamId)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
    
    var headers: [String : String]? {
        switch self {
        default:
            return ["X-Auth-Token": apiKey]
        }
    }
    
    var task: Task {
        switch self {
        default:
            return .requestPlain
        }
    }
}
