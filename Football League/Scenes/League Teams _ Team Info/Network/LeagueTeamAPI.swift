//
//  LeagueTeam.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import Foundation

final class LeagueTeamAPI: BaseAPI<LeagueTeamNetwork>, LeagueTeamAPIProtocol {
    
    //MARK:- Requests
    
    func getLeagueTeams(completion: @escaping (Result<LeagueTeamsModel?, NSError>) -> Void) {
        self.fetchData(target: .getLeagueTeams, responseClass: LeagueTeamsModel.self, completion: completion)
    }
    
    func getTeamInfo(teamId: Int, completion: @escaping (Result<TeamInfoModel?, NSError>) -> Void) {
        self.fetchData(target: .getTeamInfo(teamId: teamId), responseClass: TeamInfoModel.self, completion: completion)
    }
     
}
