//
//  LeagueTeamAPIProtocol.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import Foundation
protocol LeagueTeamAPIProtocol {
    func getLeagueTeams(completion: @escaping (Result<LeagueTeamsModel?, NSError>) -> Void)
    func getTeamInfo(teamId: Int,completion: @escaping (Result<TeamInfoModel?, NSError>) -> Void)
}
