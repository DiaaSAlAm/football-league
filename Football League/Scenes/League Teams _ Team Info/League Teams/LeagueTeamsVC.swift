//
//  LeagueTeamsVC.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import UIKit
 
final class LeagueTeamsVC: BaseViewController, LeagueTeamsViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    var presenter: LeagueTeamsPresenterProtocol!
    private let cellIdentifier = CellIdentifiers.commonCell
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCollectionView()
        apiCall()
        bind()
    }
    
    //MARK: - Register  CollectionView Cell
    fileprivate func registerCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    //MARK: - API's Call
    private func apiCall() {
        presenter.getLeagueTeams()
    }
    
    //MARK: - bind Data
    private func bind() {
        presenter.leagueTeamsModel.observe(on: self) { [weak self] _ in
            self?.collectionView.reloadData()
        }
        presenter.loadingIndicator.observe(on: self) { [weak self] (showLoading) in
            switch showLoading {
            case .showIndicator:
                self?.showLoadingIndicator()
            case .hiddenIndicator:
                self?.hideLoadingIndicator()
            }
        }
    }
}

 
//MARK: - UICollectionView Delegate
extension LeagueTeamsVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        defer { collectionView.deselectItem(at: indexPath, animated: true) }
        presenter.navigateToTeamInfo(indexPath)
    }
}

//MARK: - UICollectionView Data Source
extension LeagueTeamsVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.leagueTeamsModel.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,for: indexPath) as! CommonCell
        presenter.configureCell(cell: cell, indexPath: indexPath)
        return cell
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension LeagueTeamsVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        return CGSize(width: width , height: 80)
    }
}


 
