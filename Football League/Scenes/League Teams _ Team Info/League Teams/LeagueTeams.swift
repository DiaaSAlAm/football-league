//
//  LeagueTeams.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import UIKit

protocol LeagueTeamsViewProtocol: class { //View Conteroller
    var presenter: LeagueTeamsPresenterProtocol! { get set }
}

protocol LeagueTeamsPresenterProtocol: class { // Logic
    var view: LeagueTeamsViewProtocol? { get set }
    func configureCell(cell: CellViewProtocol, indexPath: IndexPath)
    var loadingIndicator: Observable<LoadingIndicator> { get }
    var leagueTeamsModel: Observable<[Teams]> { get }
    func navigateToTeamInfo(_ indexPath: IndexPath)
    func getLeagueTeams()
}

protocol LeagueTeamsInteractorInputProtocol: class { // func do it from presenter
     var presenter: LeagueTeamsInteractorOutputProtocol? { get set }
    func getLeagueTeams()
}

protocol LeagueTeamsInteractorOutputProtocol: class { // it's will call when interactor finished
    func fetchedSuccessfuly(model: [Teams])
    func fetchingFailed(withError error: String)
    func showMessage(message: String)
}

protocol LeagueTeamsRouterProtocol: class  { // For Segue
    var baseRouter: BaseRouterProtocol? { get set }
    func navigateToTeamInfo(teamId: Int)
     
}
