//
//  LeagueTeamsModel.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//
 
import Foundation

// MARK: - LeagueTeamsModel
struct LeagueTeamsModel: Codable {
    let teams: [Teams]?
}

// MARK: - Team
struct Teams: Codable {
    let id: Int?
    let name, shortName: String?
}
 
