//
//  LeagueTeamsPresenter.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import UIKit

final class LeagueTeamsPresenter: LeagueTeamsPresenterProtocol, LeagueTeamsInteractorOutputProtocol {
    
    weak var view: LeagueTeamsViewProtocol?
    private let interactor: LeagueTeamsInteractorInputProtocol
    private var router: LeagueTeamsRouterProtocol
    var loadingIndicator: Observable<LoadingIndicator> = Observable(.hiddenIndicator)
    var leagueTeamsModel: Observable<[Teams]> = Observable([])
    

    init(view: LeagueTeamsViewProtocol, interactor: LeagueTeamsInteractorInputProtocol, router: LeagueTeamsRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func getLeagueTeams() {
        loadingIndicator.value = .showIndicator
        interactor.getLeagueTeams()
    }
    
    func fetchedSuccessfuly(model: [Teams]) {
        loadingIndicator.value = .hiddenIndicator
        leagueTeamsModel.value = model 
    }
    
    func fetchingFailed(withError error: String) {
        loadingIndicator.value = .hiddenIndicator
        router.baseRouter?.showMessage(message: error, messageKind: .error)
    }
    
    func showMessage(message: String) {
        router.baseRouter?.showMessage(message: message, messageKind: .success)
    }
    
    func configureCell(cell: CellViewProtocol, indexPath: IndexPath) {
        guard let model = leagueTeamsModel.value.getElement(at: indexPath.row) else {return}
        let viewModel = CommonViewModel(name: model.name ?? "")
        cell.configureCell(viewModel: viewModel)
    }
     
    
    func navigateToTeamInfo(_ indexPath: IndexPath){ 
        guard let teamId = leagueTeamsModel.value.getElement(at: indexPath.row)?.id else {return}
        router.navigateToTeamInfo(teamId: teamId)
    }
}
