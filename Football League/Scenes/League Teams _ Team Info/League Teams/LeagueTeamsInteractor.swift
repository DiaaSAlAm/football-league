//
//  LeagueTeamsInteractor.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import Foundation

final class LeagueTeamsInteractor: LeagueTeamsInteractorInputProtocol {

    weak var presenter: LeagueTeamsInteractorOutputProtocol?
    private let leagueTeamNetwork: LeagueTeamAPIProtocol = LeagueTeamAPI()
    
    func getLeagueTeams(){
        leagueTeamNetwork.getLeagueTeams { [weak self] (result) in
          guard let self = self else {return}
          switch result {
          case .success(let response):
            guard let model = response?.teams , model.count != 0 else {
                self.presenter?.fetchingFailed(withError: StaticMessages.genericErrorMessage)
                return}
            self.presenter?.fetchedSuccessfuly(model: model)
          case .failure(let error):
              let err = error.localizedDescription
              print(err)
            self.presenter?.fetchingFailed(withError: err)
          }
        }
    }
     
}
