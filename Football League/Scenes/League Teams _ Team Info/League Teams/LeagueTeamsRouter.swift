//
//  LeagueTeamsRouter.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import UIKit
 
final class LeagueTeamsRouter: LeagueTeamsRouterProtocol {
    
    //Router is only one take dicret instance
    weak var viewController: UIViewController?
    var baseRouter: BaseRouterProtocol? = BaseRouter()
    
    static func createModule() -> UIViewController {
        let view = UIStoryboard(name: StoryboardNames.main, bundle: nil).instantiateViewController(withIdentifier: "\(LeagueTeamsVC.self)") as! LeagueTeamsVC
        let interactor = LeagueTeamsInteractor()
        let router = LeagueTeamsRouter()
        let presenter = LeagueTeamsPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }
    
    func navigateToTeamInfo(teamId: Int) {
        let viewController = TeamInfoRouter.createModule(teamId: teamId)
        self.viewController?.present(viewController, animated: true, completion: nil)
    }
}
