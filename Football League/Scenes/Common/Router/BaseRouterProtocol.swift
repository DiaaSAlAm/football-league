//
//  BaseRouterProtocol.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import Foundation

protocol BaseRouterProtocol: class {
    func showMessage(message: String, messageKind: ToastMessageKind)
}
