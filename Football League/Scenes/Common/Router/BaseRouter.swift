//
//  BaseRouter.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import Foundation

final class BaseRouter: BaseRouterProtocol {
    func showMessage(message: String, messageKind: ToastMessageKind) {
        DispatchQueue.main.async {
            ToastManager.shared.showMessage(messageKind: messageKind, message: message)
        }
    }
}
