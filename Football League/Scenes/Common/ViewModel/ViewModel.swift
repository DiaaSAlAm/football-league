//
//  ViewModel.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import Foundation

struct CommonViewModel {
    var name: String?
    
    init(name: String) {
        self.name = name
    }
}
