//
//  CommonCell.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import UIKit

class CommonCell: UICollectionViewCell, CellViewProtocol {

    //MARK: - IBOutlets
    @IBOutlet private weak var nameLabel: UILabel!
    
    //MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(viewModel: CommonViewModel){
        nameLabel.text = viewModel.name
    }

}
