//
//  LoadingIndicator.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import Foundation

enum LoadingIndicator {
     case showIndicator , hiddenIndicator
 }
