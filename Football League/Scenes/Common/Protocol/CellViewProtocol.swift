//
//  CellViewProtocol.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import Foundation

protocol CellViewProtocol { /// it's will call when dequeue Reusable Cell
    func configureCell(viewModel: CommonViewModel)
}
