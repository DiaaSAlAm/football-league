//
//  ToastMessageKind.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/28/21.
//


import Foundation

 enum ToastMessageKind {
     case info , error, success
 }
