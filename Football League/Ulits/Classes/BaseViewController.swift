//
//  BaseViewController.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/28/21.
//

import UIKit

class BaseViewController: UIViewController {
    
    //MARK: - Properties
    private let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    private let greyView = UIView()
    private let activityView = UIActivityIndicatorView(style: .large)
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showLoadingIndicator() {
        DispatchQueue.main.async { [weak self] in
            self?.startAnimatingActivityView()
        }
    }
    
    func hideLoadingIndicator() {
        DispatchQueue.main.async { [weak self] in
            self?.stopAnimatingActivityView()
        }
    }
    
    //MARK: - Start Animating Activity
    private func startAnimatingActivityView() {
        view.isUserInteractionEnabled = false
        greyView.frame = view.frame
        greyView.backgroundColor = .white
        greyView.alpha = 0.6
        view.addSubview(greyView)
        
        activityView.hidesWhenStopped = true
        activityView.center = view.center
        activityView.startAnimating()
        view.addSubview(activityView)
    }
    
    //MARK: - Stop Animating Activity
    private func stopAnimatingActivityView() {
        view.isUserInteractionEnabled = true
        greyView.removeFromSuperview()
        activityView.stopAnimating()
    }
}

 
