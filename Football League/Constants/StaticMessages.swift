//
//  StaticMessages.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import Foundation

struct StaticMessages {
    static let genericErrorMessage = "Opps! Looks like something went wrong."
}

