//
//  ConstantsColors.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/29/21.
//

import UIKit

struct ConstantsColors {
    static let mainColor = UIColor(named: "mainColor")
    static let backgroundColor = UIColor(named: "backgroundColor")
}

