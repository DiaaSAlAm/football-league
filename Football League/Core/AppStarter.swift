//
//  AppStarter.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/28/21.
//

import Foundation 

/// AppStarter here you can handle everything before letting your app starts
final class AppStarter {
    static let shared = AppStarter()
    private let window = (SceneDelegate.shared?.window)
    
    private init() {}
    
    func start() {
        AppTheme.apply()
        setRootViewController()
    }
    
    private func setRootViewController() {
        let view = LeagueTeamsRouter.createModule()
        window?.rootViewController = view
        window?.makeKeyAndVisible()
    }
}
