//
//  AppTheme.swift
//  Football League
//
//  Created by Diaa SAlAm on 7/28/21.
//

import UIKit

/// Apply application theme and colors
class AppTheme {
    
    static func apply() {
        navigationBarStyle() 
    }
    
    private static func navigationBarStyle() {
        UINavigationBar.appearance().barTintColor = ConstantsColors.mainColor
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        UINavigationBar.appearance().isTranslucent = false
    }
}
