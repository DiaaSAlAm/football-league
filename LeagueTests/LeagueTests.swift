//
//  LeagueTests.swift
//  LeagueTests
//
//  Created by Diaa SAlAm on 7/29/21.
//

import XCTest
@testable import Football_League

class LeagueTests: XCTestCase {
    
    var presenter: LeagueTeamsPresenter?
    let mockInteractor = LeagueTeamsInteractor()
    let mockRouter = MockRouter()
    var mockInterface = MockInterface()
    var fakeLeagueTeamsModel = [Teams(id: 0, name: "name 0", shortName: "shortName 0"), Teams(id: 1, name: "name 1", shortName: "shortName 1"), Teams(id: 2, name: "name 2", shortName: "shortName 2")]
    override func setUp() {
        presenter = LeagueTeamsPresenter(view: mockInterface, interactor: mockInteractor, router: mockRouter)
        mockInterface.presenter = presenter
        mockInteractor.presenter = presenter
    }
    
    func testRowsCountIs3() {
        presenter?.fetchedSuccessfuly(model: fakeLeagueTeamsModel)
        XCTAssertEqual(presenter?.leagueTeamsModel.value.count, 3)
    }
    
    func testRowsAtIndexIsInjectedModel() {
        presenter?.fetchedSuccessfuly(model: fakeLeagueTeamsModel)
        let model = presenter?.leagueTeamsModel.value.getElement(at: 0)
        XCTAssertEqual(model?.name, fakeLeagueTeamsModel[0].name)
        XCTAssertEqual(model?.id, fakeLeagueTeamsModel[0].id)
    }
    
    func testRowsEmptyShouldShowError() {
        presenter?.fetchingFailed(withError: "")
        XCTAssertEqual(mockInterface.loadingIndicator.value,
                       .hiddenIndicator)
    }
    
    func testSelectedRows() {
        presenter?.fetchedSuccessfuly(model: fakeLeagueTeamsModel)
        let indexPath = IndexPath(row: 0, section: 0)
        presenter?.navigateToTeamInfo(indexPath)
        XCTAssertEqual(mockRouter.teamId, fakeLeagueTeamsModel[0].id)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

final class MockRouter: LeagueTeamsRouterProtocol {
     var teamId = 0
    var baseRouter: BaseRouterProtocol?
    
    func navigateToTeamInfo(teamId: Int) {
        self.teamId = teamId
    }
}


final class MockInterface: LeagueTeamsViewProtocol {
    var presenter: LeagueTeamsPresenterProtocol!
    var loadingIndicator: Observable<LoadingIndicator> = Observable(.hiddenIndicator)
    
    func bind(){
        presenter.loadingIndicator.observe(on: self) { [weak self] (showLoading) in
            switch showLoading {
            case .showIndicator:
                self?.loadingIndicator.value = .showIndicator
            case .hiddenIndicator:
                self?.loadingIndicator.value = .hiddenIndicator
            }
        }
    }
}


final class MockTask: URLSessionDataTask {
    private let data: Data?
    private let urlResponse: URLResponse?

    private let _error: Error?
    override var error: Error? {
        return _error
    }

    var completionHandler: ((Data?, URLResponse?, Error?) -> Void)!

    init(data: Data?, urlResponse: URLResponse?, error: Error?) {
        self.data = data
        self.urlResponse = urlResponse
        self._error = error
    }

    override func resume() {
        DispatchQueue.main.async {
            self.completionHandler(self.data, self.urlResponse, self.error)
        }
    }
}
